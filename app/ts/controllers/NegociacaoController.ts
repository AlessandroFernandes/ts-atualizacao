import { Negociacoes, Negociacao} from '../models/Index';
import { MensagemView, NegociacaoView} from '../views/Index';

export default class NegociacaoController 
{

    private _data: HTMLInputElement;
    private _quantidade: HTMLInputElement;
    private _valor: HTMLInputElement;
    private _negociacoes = new Negociacoes();
    private _negociacaoView = new NegociacaoView("#updateTabela");
    private _mensagemView = new MensagemView("#mensagemView");

    constructor()
    {
        this._data = <HTMLInputElement>document.querySelector("#data");
        this._quantidade = <HTMLInputElement>document.querySelector('#quantidade');
        this._valor = <HTMLInputElement>document.querySelector("#valor");
        this._negociacaoView.Update(this._negociacoes);
    }

    public Adiciona(event: Event): void
    {
        event.preventDefault();
        this.NovaNegocicao();
        this._negociacoes.Adiciona(this.NovaNegocicao());
        this.LimparFormulario();
        this._negociacaoView.Update(this._negociacoes);
        this._mensagemView.Update("Negociação Criada com Sucesso!");
    }

    private NovaNegocicao() : Negociacao
    {
        return new Negociacao(
                              new Date(this._data.value.replace(/-/g, ',')),
                              parseInt(this._quantidade.value),
                              parseFloat(this._valor.value),
                             );
    }
    
    private LimparFormulario() : void
    {
        document.querySelector("form").reset();
        this._data.focus();
    }
}

