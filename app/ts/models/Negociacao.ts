export  class Negociacao
{
    constructor(readonly Data : Date, readonly Quantidade: number, readonly Valor: number){}

    get Total() {
        return this.Quantidade * this.Valor;
    }
}
