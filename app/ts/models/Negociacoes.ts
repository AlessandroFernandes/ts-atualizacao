import { Negociacao } from "./Negociacao";

export class Negociacoes 
{
    private readonly _negociacoes : Array<Negociacao> = [];

    Adiciona(negociacao : Negociacao) : void
    {
        this._negociacoes.push(negociacao);
    }

    TodasNegociacoes() : Negociacao[]
    {
        return [].concat(this._negociacoes);
    }
}