import { View } from './View';

export class MensagemView extends View<string>
{
    Template(negociacoes: string) : string{
        return `<p class="alert alert-success">${negociacoes}</p>`
    }
}