export abstract class View<T>
{
    protected readonly _element : Element;

    constructor(seletor: string)
    {
        this._element = document.querySelector(seletor);
    }

    public Update(negociacoes : T) : void
    {
        this._element.innerHTML = this.Template(negociacoes)
    }

    abstract Template(negociacoes: T) : string

}