import { View } from './View';
import { Negociacoes } from "../models/Negociacoes";

export class NegociacaoView extends View<Negociacoes>
{
    Template(negociacoes: Negociacoes) : string
    {
        return  `<table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>DATA</th>
                            <th>QUANTIDADE</th>
                            <th>VALOR</th>
                            <th>VOLUME</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                    ${negociacoes.TodasNegociacoes().map(negociacao =>
                        `   <tr>
                                <td>${negociacao.Data.getDate()}/
                                    ${negociacao.Data.getMonth()
                                                     .toString()
                                                     .length > 1 ? 
                                                     negociacao.Data.getMonth() +1 :
                                                     '0' + (negociacao.Data.getMonth() +1)}/
                                    ${negociacao.Data.getFullYear()}
                                </td>
                                <td>${negociacao.Quantidade}</td>
                                <td>${negociacao.Valor}</td>
                                <td>${negociacao.Total}</td>
                            </tr>
                        `).join('')}
                    </tbody>
                    <tfoot>
                        <tr>
                             <td colspan='3'></td>
                             <td>
                             ${negociacoes.TodasNegociacoes().reduce((total, index) => total += index.Total, 0.0)}
                             </td>
                        </tr>                    
                    </tfoot>
                </table>
                `;
    }
}